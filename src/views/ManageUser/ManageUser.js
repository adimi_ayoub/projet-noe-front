import React, { useEffect, useState,setState } from "react";
import MaterialTable from 'material-table';

import axios from 'axios';


export default function MaterialTableDemo() {
  

  const [entries, setEntries] = useState({
    data: [
        {
          
          username :"",
          email: "",
          name: "",
          lastname: "",
          password: "",
          signature:"",
          function:""
        }
    ]
});


const [state] = React.useState({
  columns: [
    
    { title: 'Username', field: 'username' },
    { title: 'Email', field: 'email' },
    { title: 'Name', field: 'name' },
    { title: 'Lastname', field: 'lastname' },
    { title: 'Password', field: 'password' },
    { title: 'Signature', field: 'signature' },
    { title: 'Function', field: 'function' }
  ]
});
    useEffect(() => {
      axios
      .get("http://localhost:8080/users")
      .then(response => {
      let data = [];
  response.data.forEach(el => {
      data.push({
      
      username:el.username,
      email: el.email,
      name: el.name,
      lastname: el.lastname,
      password :el.password,
      signature : el.signature,
      function : el.function
  });
  });
  setEntries({ data: data });
  })
  .catch(function(error) {
      console.log(error);
  });
  }, []);

  return (
    <MaterialTable
      title="Administration"
      columns={state.columns}
      data={entries.data}
      editable={{
        onRowAdd: (newData) =>
        new Promise((resolve) => {
          setTimeout(() => {
            resolve();
            const data = [...state.data];
            console.log(data);
            const payload = {
              foo: newData.foo,
              bar: newData.bar,
            };
            axios
              .post('http://localhost:8080/registration', payload, {
                headers: {
                  
                },
              })
              .then((res) => {
                console.log(res.data.data);
                const resData = [...data, res.data.data];
                setState({ ...state, data: resData });
              });
          }, 600);
        }),
      onRowUpdate: (newData, oldData) =>
      new Promise(resolve => {
          setTimeout(() => {
          resolve();
          const data = [...entries.data];
          data[data.indexOf(oldData)] = newData;
          axios
              .post("http://localhost:8080/users", newData, {
                  params: {
                      id: entries.data[0].id
                  }
              })
              .then(res => console.log(res.data));
          setEntries({ ...entries, data });
      }, 600);
  }),

      //   onRowDelete: (oldData) =>
      //     new Promise((resolve) => {
      //       setTimeout(() => {
      //         resolve();
      //         setState((prevState) => {
      //           const data = [...prevState.data];
      //           data.splice(data.indexOf(oldData), 1);
      //           return { ...prevState, data };
      //         });
      //       }, 600);
      //     }),
      }}
    />
  );
}