import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';


import MainFeaturedPost from './MainFeaturedPost';
import FeaturedPost from './FeaturedPost';
import Main from './Main';


//new component added
import Header from 'components/Header/Header.js';


import post1 from './blog-post.1.md';


const useStyles = makeStyles((theme) => ({
  mainGrid: {
    marginTop: theme.spacing(3),
  },
}));

const sections = [
    { title: 'Projets ', url: '#' },
  { title: 'Projets en cours', url: '#' },
  { title: 'Projets terminés', url: '#' },
  
 
];

const mainFeaturedPost = {
  title: 'Projet de sauvetage',
  description:
    "Projet de sauvetage d'une espèce.",
  image: 'https://famillesaine.com/wpcms/wp-content/uploads/2018/04/nature-1920x960.jpg',
  imgText: 'main image description',
  linkText: 'Continue reading…',
};

const featuredPosts = [
  {
    title: 'Alerte lancée',
    date: 'Nov 12',
    description:
      'Sauver une espèce',
    image: 'hhttps://cdn.pixabay.com/photo/2018/04/09/02/56/nature-3303082_1280.jpg',
    imageText: 'Image Text',
  },
  {
    title: 'Alerte en cours',
    date: 'Nov 11',
    description:
      'Alerte lancée depuis la France',
    image: 'https://famillesaine.com/wpcms/wp-content/uploads/2018/04/nature-1920x960.jpg',
    imageText: 'Image Text',
  },
];

const posts = [post1];

// const sidebar = {
//   title: 'About',
//   description:
//     'Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.',
//   archives: [
//     { title: 'March 2020', url: '#' },
//     { title: 'February 2020', url: '#' },
//     { title: 'January 2020', url: '#' },
//     { title: 'November 1999', url: '#' },
//     { title: 'October 1999', url: '#' },
//     { title: 'September 1999', url: '#' },
//     { title: 'August 1999', url: '#' },
//     { title: 'July 1999', url: '#' },
//     { title: 'June 1999', url: '#' },
//     { title: 'May 1999', url: '#' },
//     { title: 'April 1999', url: '#' },
//   ],
//   social: [
//     { name: 'GitHub', icon: GitHubIcon },
//     { name: 'Twitter', icon: TwitterIcon },
//     { name: 'Facebook', icon: FacebookIcon },
//   ],
// };

export default function Blog() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header title="L'ARCHE" sections={sections} />
        <main>
          <MainFeaturedPost post={mainFeaturedPost} />
          <Grid container spacing={4}>
            {featuredPosts.map((post) => (
              <FeaturedPost key={post.title} post={post} />
            ))}
          </Grid>
          
        </main>
      </Container>
      
    </React.Fragment>
  );
}
